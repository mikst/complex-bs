from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from ase.transport.calculators import TransportCalculator as TC

t = -1.0
ec = 0.0
ed = 1.0
v2 = 0.6  # hight of barrier
e_channel = 1.1

n1 = 4  # left lead (2 principal layers)
n2 = 4  # right lead (2 principal layers)
i1, i, i2 = 32, 8, 16  # nsites in left, central, right part of ext. mol.
n = i1 + i + i2  # size of scattering region


def get_h_matrix(t, ec, ed, n):
    """
    Nearest neighbor tight-binding matrix
    with hopping t and alternatic site energies ec and ed.
    n is the size
    """
    h = np.zeros((n, n))
    h.ravel()[1::n+1] = t
    h.ravel()[n::n+1] = t
    h.ravel()[::2*(n+1)] = ec
    h.ravel()[n+1::2*(n+1)] = ed
    return h


def add_constant_pot(h, v0, indices=None):
    """
    add constant potential of value v0 for the sites specified in indices
    to the Hamiltonian h
    """
    h.ravel()[::len(h)+1][indices] += v0


h1 = get_h_matrix(t, ec, ed, n1)  # left lead ham
h2 = get_h_matrix(t, ec, ed, n2)  # right lead ham
h = get_h_matrix(t, ec, ed, n)    # central region
add_constant_pot(h, v2, range(i1, i1 + i)) # the barrier of height v2

energies = np.arange(-2, 3, 0.01)
tc = TC(energies=energies,
        h1=h1,
        h2=h2,
        h=h,
        eta=1.0e-6,
        eta1=1.0e-6,
        eta2=1.0e-6)

if 1:
    t = tc.get_transmission()
    plt.plot(energies, t)
    plt.savefig('trans.png')
    plt.show()

t_n, vec_in = tc.get_left_channels(e_channel)  # eigenchannel state
print(t_n)
plt.plot(abs(vec_in[:, 0]),'-o', label=r'$|\psi_\mathrm{scat}$|')
plt.plot(np.angle(vec_in[:,0]) / np.pi / 2, '-o', label=r'phase / $2\pi$')
#plt.plot((vec_in[:, 0]).real, '--x', label='real')
#plt.plot((vec_in[:, 0]).imag, '--+', label='imag')
plt.plot((0, i1-0.5), (0, 0), 'black', lw=2, label='potential')
plt.plot((i1-0.5, i1-0.5), (0, v2), 'black', lw=2)
plt.plot((i1-0.5, i1+i+0.5), (v2, v2), 'black', lw=2)
plt.plot((i1+i+0.5, i1+i+0.5), (v2, 0), 'black', lw=2)
plt.plot((i1+i+0.5, n), (0, 0), 'black', lw=2)
plt.legend()
plt.xlabel('site number')
plt.savefig('eigchannel_state.png')
plt.show()
